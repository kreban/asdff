package servlet;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class B {

    public List<Integer> key2 = new ArrayList<>();

    public Integer getSum() {
        int sum = 0;
        for (Integer num : key2) {
            sum += num;
        }
        return sum;
    }
}
