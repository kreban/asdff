package servlet;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import util.FileUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/transform")
public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {

        response.getWriter().print("Hello!");
    }

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        String data = FileUtil.readStream(request.getInputStream());
        A key1 = new ObjectMapper().readValue(data, new TypeReference<>() {});
        Integer sum = key1.key1.getSum();
        response.getWriter().print(sum);
    }

    @Override
    public void doPut(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {
        String data = FileUtil.readStream(request.getInputStream());
        System.out.println(data);
        ObjectMapper jsonMapper = new ObjectMapper();
        A key1 = jsonMapper.readValue(data, new TypeReference<>() {});
        B key2 = key1.getKey1();
        C nums = new C(key2.getKey2());
        response.setContentType("application/json");
        response.getWriter().print(jsonMapper.writeValueAsString(nums));
    }

}

