package spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "spring" })
@PropertySource("classpath:/application.properties")
public class Config {

    @Bean
    public UserService getUserService( ) {
        return UserServiceBuilder.build(new UserDao());
    }
}