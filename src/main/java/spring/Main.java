package spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        var ctx = new AnnotationConfigApplicationContext(Config.class);

        UserService service = ctx.getBean(UserService.class);

        System.out.println(service.getAdminUserName()); // alice


        // näide profiili vahetamisest
        ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("p2");
        ctx.register(Config.class);
        ctx.refresh();

        service = ctx.getBean(UserService.class);

        System.out.println(service.getAdminUserName()); // bob
    }
}