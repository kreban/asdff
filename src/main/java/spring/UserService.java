package spring;

public interface UserService {

    String getAdminUserName();

    UserDao getDao();

}
