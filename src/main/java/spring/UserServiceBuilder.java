package spring;

public class UserServiceBuilder {

    public static UserService build(UserDao dao) {
        return new MyUserService(dao, "admin");
    }

    public static UserService build(UserDao dao, String adminUserName) {
        return new MyUserService(dao, adminUserName);
    }

    public static boolean isFromBuilder(UserService dataSource) {
        return dataSource instanceof MyUserService;
    }

    private static class MyUserService implements UserService {

        private UserDao dao;
        private String adminUserName;

        public MyUserService(UserDao dao, String adminUserName) {
            this.dao = dao;
            this.adminUserName = adminUserName;
        }

        @Override
        public String getAdminUserName() {
            return adminUserName;
        }

        @Override
        public UserDao getDao() {
            return dao;
        }
    };

}
