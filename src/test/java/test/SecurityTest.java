package test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import runner.NoPointsIfThisTestFails;
import runner.Points;
import security.SecurityConfig;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { SecurityConfig.class })
public class SecurityTest {

    private WebApplicationContext wac;

    @Autowired
    public void setWac(WebApplicationContext wac) {
        this.wac = wac;
    }

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    @Points(1)
    public void accessToAdminIsRestricted() throws Exception {
        mvc.perform(get("/admin")).andExpect(isRestricted());
    }

    @Test
    @Points(1)
    public void showsCorrectStatusAndHeader() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/admin"))
                .andReturn()
                .getResponse();

        String header = response.getHeader("WWW-Authenticate");
        System.out.println(header);
        assertThat(header, is("Basic"));
        assertThat(response.getStatus(), is(401));
    }

    @Test
    @NoPointsIfThisTestFails
    public void canNotLogInWithWrongCredentials() throws Exception {
        mvc.perform(get("/admin").with(httpBasic("user","password")))
                .andExpect(isRestricted());
    }

    @Test
    @Points(6)
    public void canLogInWithCorrectCredentials() throws Exception {
        mvc.perform(get("/admin").with(httpBasic("alice","123")))
                .andExpect(status().isOk());
    }

    private ResultMatcher isRestricted() {
        return result -> assertThat(result.getResponse().getStatus(),
                    anyOf(is(401), is(403)));
    }

}