package test;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import runner.Points;
import servlet.MyServlet;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ServletTest {

    MockHttpServletRequest request = new MockHttpServletRequest();
    MockHttpServletResponse response = new MockHttpServletResponse();

    @Test
    @Points(16)
    public void canHandleInputJson() throws Exception {

        String json = """
           {"key1":{"key2":[1,5,7]}}""";

        request.setContent(json.getBytes(StandardCharsets.UTF_8));

        new MyServlet().doPost(request, response);

        String actual = response.getContentAsString()
                .replaceAll("[\n\r]", "");

        assertThat(actual, equalTo("13"));
    }

    @Test
    @Points(8)
    public void canConstructOutputJson() throws Exception {

        String json = """
           {"key1":{"key2":[1,5,7]}}""";

        request.setContent(json.getBytes(StandardCharsets.UTF_8));

        new MyServlet().doPut(request, response);

        String expected = """
                {"result":[1,5,7]}""";

        String actual = response.getContentAsString()
                .replaceAll(" ", "").replaceAll("[\n\r]", "");

        assertThat(actual, equalTo(expected));
    }
}