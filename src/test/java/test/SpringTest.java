package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import runner.Points;
import spring.Config;
import spring.UserDao;
import spring.UserService;
import spring.UserServiceBuilder;
import util.PropertyLoader;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class })
public class SpringTest {

    @Test
    @Points(3)
    public void constructBeanWidthBuilder() {
        var ctx = new AnnotationConfigApplicationContext(Config.class);

        UserService service = ctx.getBean(UserService.class);

        assertTrue(UserServiceBuilder.isFromBuilder(service));
    }

    @Test
    @Points(3)
    public void dependencyIsCreatedBySpring() {
        var ctx = new AnnotationConfigApplicationContext(Config.class);

        UserService service = ctx.getBean(UserService.class);

        assertThat(service.getDao(), sameInstance(ctx.getBean(UserDao.class)));
    }

    @Test
    @Points(4)
    public void hasUsernameFromPropertyFile() {
        var ctx = new AnnotationConfigApplicationContext(Config.class);

        UserService service = ctx.getBean(UserService.class);

        String expectedName = PropertyLoader.loadApplicationProperties(
                "application.properties").getProperty("adminUserName");

        assertThat(service.getAdminUserName(), is(expectedName));
    }

    @Test
    @Points(5)
    public void canSelectCorrectProfile() {
        var ctx = getContextWithProfile("p2", Config.class);

        var service = ctx.getBean(UserService.class);

        var expectedName = PropertyLoader.loadApplicationProperties(
                "application2.properties").getProperty("adminUserName");

        assertThat(service.getAdminUserName(), is(expectedName));
    }

    private AnnotationConfigApplicationContext getContextWithProfile(
            String profileName, Class<?> configClass) {

        var ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles(profileName);
        ctx.register(configClass);
        ctx.refresh();
        return ctx;
    }

}